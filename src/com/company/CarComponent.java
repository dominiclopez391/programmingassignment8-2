package com.company;

import javax.swing.*;
import java.awt.*;

public class CarComponent extends JComponent {

    Car car1;

    public CarComponent() {
        car1 = new Car(0, 0);
    }

    public void paintComponent(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        car1.draw(g2);
    }

    public void moveCar(int x, int y) {
        car1.moveCarBy(x, y);
    }

}
