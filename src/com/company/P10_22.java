package com.company;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class P10_22 {

    public static void main(String[] args) {

        int frameWidth = 400, frameHeight = 300;

        JFrame frame = new JFrame();
        frame.setSize(frameWidth, frameHeight);

        CarComponent c = new CarComponent();
        c.setSize(400, 300);

        int destinationX = frameWidth - 60;
        int destinationY = frameHeight - 30;

        class CarMover implements ActionListener {

            @Override
            public void actionPerformed(ActionEvent e) {
                c.moveCar(4, 3);
                c.repaint();
            }
        }

        Timer t = new Timer(100, new CarMover());
        t.start();

        frame.add(c);
        frame.setVisible(true);

    }
}
