package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class E10_18 {

    public static void main(String[] args) {

        JFrame frame = new JFrame();
        frame.setSize(500, 150);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JButton buttonCount1 = new JButton("I was clicked 0 times!");
        JButton buttonCount2 = new JButton("I was clicked 0 times!");

        buttonCount1.setSize(new Dimension(60, 200));
        buttonCount2.setSize(new Dimension(60, 200));


        class ButtonCounterListener implements ActionListener {

            JButton b;
            int count = 0;

            public ButtonCounterListener(JButton b) {
                this.b = b;
            }

            @Override
            public void actionPerformed(ActionEvent e) {

                count++;
                b.setText("I was clicked " + count + " times!");

            }
        }

        buttonCount1.addActionListener(new ButtonCounterListener(buttonCount1));
        buttonCount2.addActionListener(new ButtonCounterListener(buttonCount2));

        frame.add(buttonCount1, BorderLayout.WEST);
        frame.add(buttonCount2, BorderLayout.EAST);


        frame.setVisible(true);

    }



}
