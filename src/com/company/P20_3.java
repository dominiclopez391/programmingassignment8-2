package com.company;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.text.ParseException;

public class P20_3 {

    public static void main(String[] args) {

        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setLayout(new GridLayout(4, 1));

        JPanel initialSavings = new JPanel();
        JLabel savingsLabel = new JLabel("Initial Savings ($): ");
        savingsLabel.setPreferredSize(new Dimension(130, 20));
        JTextField savingsField = new JTextField(30);

        initialSavings.add(savingsLabel);
        initialSavings.add(savingsField);

        JPanel annualInterestRate = new JPanel();
        JLabel interestLabel = new JLabel("Annual Interest (%): ");
        interestLabel.setPreferredSize(new Dimension(130, 20));
        JTextField interestField = new JTextField(30);

        annualInterestRate.add(interestLabel);
        annualInterestRate.add(interestField);

        JPanel numberOfYears = new JPanel();

        JLabel yearsLabel = new JLabel("Number of years: ");
        yearsLabel.setPreferredSize(new Dimension(130, 20));
        JTextField yearsField = new JTextField(30);

        numberOfYears.add(yearsLabel);
        numberOfYears.add(yearsField);

        JPanel calculateAndResult = new JPanel();
        JButton button = new JButton("Calculate");
        JLabel result = new JLabel();

        class CalculateResultListener implements ActionListener {


            NumberFormat formatter = NumberFormat.getCurrencyInstance();

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Double initSavings = Double.parseDouble(savingsField.getText());
                    Double annualInterest = Double.parseDouble(interestField.getText()) * 0.01;
                    Double years = Double.parseDouble(yearsField.getText());

                    Double amount = initSavings * Math.pow((1 + annualInterest), years);
                    result.setText(formatter.format(amount));

                }
                catch(NumberFormatException exception) {
                    result.setText("Invalid value");
                }
            }
        }
        button.addActionListener(new CalculateResultListener());

        calculateAndResult.add(button);
        calculateAndResult.add(result);

        frame.add(initialSavings);
        frame.add(annualInterestRate);
        frame.add(numberOfYears);
        frame.add(calculateAndResult);

        frame.pack();
        frame.setVisible(true);

    }

}
